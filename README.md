Requirements:
python>=3.7
tensorflow>=1.15


Datasets:
miniImagenet and tieredImagenet:https://github.com/renmengye/few-shot-ssl-public

CIFAR-FS:https://github.com/bertinetto/r2d2


Train command:
python3 train.py --n_way=5 --n_shot=1 --n_test_way=5 --n_test_shot=1 --lr=1e-3 --step_size=10000 --dataset=mini --exp_name=mini_5w1s_5tw1ts_rn300_k20-tpn --rn=300 --alpha=0.99 --k=20 --encoder=conv --feat_select=no --infer=tpn

