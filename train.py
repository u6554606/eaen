from   __future__ import print_function
from   PIL import Image
import numpy as np
import os
from   models import *
from   dataset_mini import *
from   dataset_cifar import *
from   dataset_tiered import *
from   tqdm import tqdm
import argparse
import random
#import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()


parser = argparse.ArgumentParser(description='Train TPN')

# parse gpu 
parser.add_argument('--gpu',        type=str,    default=1,     metavar='GPU',
                    help="gpu name, default:0")

# model params
n_examples = 600
parser.add_argument('--x_dim',      type=str,    default="84,84,3", metavar='XDIM',
                    help='input image dims')
parser.add_argument('--im_size',      type=int,    default=5,    metavar='HDIM',
                    help="image size after pooling (default: 5)")
parser.add_argument('--h_dim',      type=int,    default=64,    metavar='HDIM',
                    help="channels of hidden conv layers (default: 64)")
parser.add_argument('--z_dim',      type=int,    default=64,    metavar='ZDIM',
                    help="channels of last conv layer (default: 64)")

# training hyper-parameters
n_episodes = 100
n_val = 40
parser.add_argument('--n_way',      type=int,    default=5,     metavar='NWAY',
                    help="nway")
parser.add_argument('--n_shot',     type=int,    default=5,     metavar='NSHOT',
                    help="nshot")
parser.add_argument('--n_query',    type=int,    default=15,    metavar='NQUERY',
                    help="nquery")
parser.add_argument('--n_epochs',   type=int,    default=1600,  metavar='NEPOCHS',
                    help="nepochs")
# test hyper-parameters
parser.add_argument('--n_test_way', type=int,    default=5,     metavar='NTESTWAY',
                    help="ntestway")
parser.add_argument('--n_test_shot',type=int,    default=5,     metavar='NTESTSHOT',
                    help="ntestshot")
parser.add_argument('--n_test_query',type=int,   default=15,    metavar='NTESTQUERY',
                    help="ntestquery")

# optimization params
parser.add_argument('--lr',         type=float,  default=0.001, metavar='LR',
                    help="base learning rate")
parser.add_argument('--step_size',  type=int,    default=10000, metavar='DSTEP',
                    help="step_size")
#parser.add_argument('--step_size',  type=int,    default=10000, metavar='DSTEP',
#                    help="step_size")
parser.add_argument('--gamma',      type=float,  default=0.5,   metavar='DRATE',
                    help="gamma")
parser.add_argument('--patience',   type=int,    default=200,   metavar='PATIENCE',
                    help="patience")

# dataset params
parser.add_argument('--dataset',    type=str,    default='mini',metavar='DATASET',
                    help="mini or tiered")
parser.add_argument('--ratio',      type=float,  default=1.0,   metavar='RATIO',
                    help="ratio of labeled data (for semi-supervised setting")
parser.add_argument('--pkl',        type=int,    default=1,     metavar='PKL',
                    help="1 for use pkl dataset, 0 for original images")

# label propagation params
parser.add_argument('--k',          type=int,    default=20,    metavar='K',
                    help="top k in constructing the graph W")
parser.add_argument('--sigma',      type=float,  default=0.25,  metavar='SIGMA',
                    help="sigma of graph computing parameter")
parser.add_argument('--alpha',      type=float,  default=0.99,  metavar='ALPHA',
                    help="alpha in label propagation")
parser.add_argument('--rn',         type=int,    default=300,   metavar='RN',
                    help="graph construction types: "
                    "300: sigma is learned, alpha is fixed" +
                    "30:  both sigma and alpha learned")

# seed and exp_name
parser.add_argument('--seed',       type=int,    default=1000,  metavar='SEED',
                    help="random seed, -1 means no seed")
parser.add_argument('--exp_name',   type=str,    default='exp', metavar='EXPNAME',
                    help="experiment description name")
parser.add_argument('--iters',      type=int,    default=0,    metavar='ITERS',
                    help="checkpoint restore iters")
#encoder arg
parser.add_argument('--semi_ratio',      type=float,  default=1.0,   metavar='SEMIRATIO',
                    help="ratio of labeled data")
parser.add_argument('--encoder',   type=str,    default='conv', metavar='ENCODER',
                    help="encoder name")
parser.add_argument('--feat_select',   type=str,    default='single', metavar='SELECT',
                    help="feature select method")
parser.add_argument('--infer',   type=str,    default='tpn', metavar='INFER',
                    help="inference method")



# deal with params
args = vars(parser.parse_args())
im_width, im_height, channels = list(map(int, args['x_dim'].split(',')))
args['im_size'] = int(np.floor(im_width/16))
for key,v in args.items(): exec(key+'=v')

## RANDOM SEED
#random.seed(seed)
#np.random.seed(seed)
#tf.set_random_seed(seed)

# set environment variables
os.environ["CUDA_VISIBLE_DEVICES"] = args['gpu']
is_training = True


# deal with checkpoints save folder
def _init_():
    if not os.path.exists('checkpoints'):
        os.makedirs('checkpoints')
    if not os.path.exists('checkpoints/'+args['exp_name']):
        os.makedirs('checkpoints/'+args['exp_name'])
    if not os.path.exists('checkpoints/'+args['exp_name']+'/'+'models'):
        os.makedirs('checkpoints/'+args['exp_name']+'/'+'models')
    if not os.path.exists('checkpoints/'+args['exp_name']+'/'+'summaries'):
        os.makedirs('checkpoints/'+args['exp_name']+'/'+'summaries')
    os.system('cp train.py checkpoints'+'/'+args['exp_name']+'/'+'train.py.backup')
    os.system('cp models.py checkpoints' + '/' + args['exp_name'] + '/' + 'models.py.backup')
    f = open('checkpoints/'+args['exp_name']+'/log.txt', 'a')
    print(args, file=f)
    f.close()
_init_()


# construct dataset
if dataset=='mini':
    loader_train = dataset_mini(n_examples, n_episodes, 'train', args)
    loader_val   = dataset_mini(n_examples, n_episodes, 'val', args)
elif dataset=='tiered':
    loader_train = dataset_tiered(n_examples, n_episodes, 'train', args)
    loader_val   = dataset_tiered(n_examples, n_episodes, 'val', args)
elif dataset=='cifar':
    loader_train = dataset_cifar(n_examples, n_episodes, 'train', args)
    loader_val   = dataset_cifar(n_examples, n_episodes, 'val', args)

if pkl==0:
    print('Load image data rather than PKL')
    loader_train.load_data()
    loader_val.load_data()
else:
    print('Load PKL data')
    loader_train.load_data_pkl()
    loader_val.load_data_pkl()


# construct model
m = models(args)
ce_loss,acc,sigma_value, debug_info = m.construct()


# train and stepsize
global_step   = tf.Variable(0, name="global_step", trainable=False)
learning_rate = tf.train.exponential_decay(lr, global_step,
                                                step_size, gamma, staircase=True)
# update ops for batch norm
update_ops    = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    optimizer = tf.train.AdamOptimizer(learning_rate)
    grads_and_vars = optimizer.compute_gradients(ce_loss)
    for g,v in grads_and_vars:
        print('grad: {} -- {}'.format(v.name, g))
    train_op  = optimizer.minimize(ce_loss, global_step=global_step)

# init session and start training
config  = tf.ConfigProto()  
config.gpu_options.allow_growth=True 
sess    = tf.Session(config=config)

init_op = tf.global_variables_initializer()
sess.run(init_op)


# summary
save_dir = 'checkpoints/'+args['exp_name']

loss_summary  = tf.summary.scalar("loss", ce_loss)
acc_summary   = tf.summary.scalar("accuracy", acc)
lr_summary    = tf.summary.scalar("lr", learning_rate)
sigma_summary = tf.summary.histogram("sigma", sigma_value)

train_summary_op     = tf.summary.merge([loss_summary, acc_summary, lr_summary, sigma_summary])
train_summary_dir    = os.path.join(save_dir, "summaries", "train")
train_summary_writer = tf.summary.FileWriter(train_summary_dir, sess.graph)

val_summary_op     = tf.summary.merge([loss_summary, acc_summary, sigma_summary])
val_summary_dir    = os.path.join(save_dir, "summaries", "val")
val_summary_writer = tf.summary.FileWriter(val_summary_dir, sess.graph)


# restore pre-trained model
saver      = tf.train.Saver(tf.global_variables(), max_to_keep=3)
model_path = save_dir+'/models'
if iters>0:
    ckpt_path = model_path+'/ckpt-'+str(iters)
    saver.restore(sess, ckpt_path)
    print('Load model from {}'.format(ckpt_path))


# Train and Val stages
best_acc  = 0
best_loss = np.inf
wait      = 0

for ep in range(int(iters/100), n_epochs):
    loss_tr  = []
    acc_tr   = []
    loss_val = []
    acc_val  = []
    # run episodes training and then val
    for epi in tqdm(range(n_episodes), desc='train epoc:{}'.format(ep)):
        if ratio==1.0:
            support, s_labels, query, q_labels, _ = loader_train.next_data(n_way, n_shot, n_query)
        else:
            support, s_labels, query, q_labels, _ = loader_train.next_data_un(n_way, n_shot, n_query)

        if semi_ratio < 1.0:
            s_labels[:, int(args['n_shot'] * args['semi_ratio']):] = -1
#            else:
#                support = support[:,0:int(args['n_shot'] * args['semi_ratio'])]
#                s_labels = s_labels[:,0:int(args['n_shot'] * args['semi_ratio'])]

        _, summaries, step, ls, ac, debug_val = sess.run([train_op, train_summary_op, global_step, ce_loss, acc,debug_info], feed_dict={m.x: support, m.ys:s_labels, m.q: query, m.y:q_labels, m.phase:True})

#        if epi > 0 and epi % (n_episodes - 2) == 0:
#            for g,v in grads_and_vars:
#                print('{} {} -- {}'.format(epi, v.name, sess.run(g, feed_dict={m.x: support, m.ys:s_labels, m.q: query, m.y:q_labels, m.phase:1})))
        
        train_summary_writer.add_summary(summaries, step)
        loss_tr.append(ls)
        acc_tr.append(ac)

    #print('logits: {} label: {} loss: {}'.format(debug_val[0], debug_val[1], debug_val[2]))
    #print('loss debug: lp:{} lnp:{} diff: {} total:{}'.format(debug_val[0], debug_val[1], debug_val[2], debug_val[3]))

    # validation after each episode training, and decide if stop after train_patience steps
    for epi in tqdm(range(n_val), desc='val epoc:{}'.format(ep)):
        # validation to decide if stop
        support, s_labels, query, q_labels, _ = loader_val.next_data(n_test_way, n_test_shot, n_test_query, train=False)
        if semi_ratio < 1.0:
            s_labels[:, int(args['n_shot'] * args['semi_ratio']):] = -1
#            else:
#                support = support[:,0:int(args['n_shot'] * args['semi_ratio'])]
#                s_labels = s_labels[:,0:int(args['n_shot'] * args['semi_ratio'])]
        
        summaries, vls, vac, debug_val = sess.run([val_summary_op, ce_loss, acc, debug_info], feed_dict={m.x: support, m.ys:s_labels, m.q: query, m.y:q_labels, m.phase:False})

        val_summary_writer.add_summary(summaries, step)
        loss_val.append(vls)
        acc_val.append(vac)
        
    #print('logits: {} label: {} loss: {}'.format(debug_val[0], debug_val[1], debug_val[2]))
    print('epoch:{}, loss:{:.5f}, acc:{:.5f}, val, loss:{:.5f}, acc:{:.5f}'.format(ep, np.mean(loss_tr), np.mean(acc_tr), np.mean(loss_val), np.mean(acc_val)))


    # Model save and stop criterion
    cond1 = (np.mean(acc_val)>best_acc)
    cond2 = (np.mean(loss_val)<best_loss)
    if cond1 or cond2:
        best_acc  = np.maximum(np.mean(acc_val),  best_acc)
        best_loss = np.minimum(np.mean(loss_val), best_loss)
        print('best val loss:{:.5f}, acc:{:.5f}'.format(best_loss, best_acc))
        
        # save the model
        saver.save(sess, model_path+'/ckpt', global_step=step)
        wait = 0
        
        f = open('checkpoints/'+args['exp_name']+'/log.txt', 'a')
        print('{} {:.5f} {:.5f}'.format(step, np.mean(loss_val), np.mean(acc_val)), file=f)
        f.close()
#    else:
#        wait += 1
#        if ep%100==0:
#            saver.save(sess, model_path+'/ckpt', global_step=step)
#            f = open('checkpoints/'+args['exp_name']+'/log.txt', 'a')
#            print('{} {:.5f} {:.5f}'.format(step, np.mean(loss_val), np.mean(acc_val)), file=f)
#            f.close()

    if wait>patience and ep>n_epochs and rn>=0:
        break


