from   __future__ import print_function
import numpy as np
from   PIL import Image
import pickle as pkl
import os
import glob
import csv

class dataset_cifar(object):
    def __init__(self, n_examples, n_episodes, split, args):
        #self.im_width, self.im_height, self.channels = list(map(int, args['x_dim'].split(',')))
        self.im_width, self.im_height, self.channels = [32,32,3]
        self.n_examples = n_examples
        self.n_episodes = n_episodes
        self.split      = split
        self.ratio      = args['ratio']
        self.seed       = args['seed']
        self.root_dir   = './data/cifar'
        
        self.n_label    = int(self.ratio*self.n_examples)
        self.n_unlabel  = self.n_examples-self.n_label
        self.dataset_l  = []
        self.dataset_u  = []

        self.args = args

    def build_label_index(self, labels):
        label2inds = {}
        for idx, label in enumerate(labels):
            if label not in label2inds:
                label2inds[label] = []
            label2inds[label].append(idx)
        return label2inds


    def load_data_pkl(self):
        """
            load the pkl processed mini-imagenet into label,unlabel
        """
        pkl_name = '{}/CIFAR_FS_{}.pickle'.format(self.root_dir, self.split)
        print('Loading pkl dataset: {} '.format(pkl_name))

        try:
          with open(pkl_name, "rb") as f:
            data         = pkl.load(f, encoding='bytes')
            image_data   = data[b'data']
            label_data   = data[b'labels']
        except:
          with open(pkl_name, "rb") as f:
            data         = pkl.load(f)
            image_data   = data['data']
            label_data   = data['labels']

        data_classes = self.build_label_index(label_data)
        #print(data.keys(), image_data.shape, class_dict.keys())
        class_list     = sorted(data_classes.keys()) # sorted to keep the order

        n_classes        = len(class_list)
        print('n_classes:{}, n_label:{}, n_unlabel:{}'.format(n_classes,self.n_label,self.n_unlabel))
        dataset_l        = np.zeros([n_classes, self.n_label, self.im_height, self.im_width, self.channels], dtype=np.float32)
        if self.n_unlabel>0:
            dataset_u    = np.zeros([n_classes, self.n_unlabel, self.im_height, self.im_width, self.channels], dtype=np.float32)
        else:
            dataset_u    = []

        for i, cls in enumerate(class_list):
            idxs         = data_classes[cls] 
            np.random.RandomState(self.seed).shuffle(idxs) # fix the seed to keep label,unlabel fixed
            dataset_l[i] = image_data[idxs[0:self.n_label]]
            if self.n_unlabel>0:
                dataset_u[i] = image_data[idxs[self.n_label:]]
        print('labeled data:', np.shape(dataset_l))
        print('unlabeled data:', np.shape(dataset_u))
    
        self.dataset_l   = dataset_l
        self.dataset_u   = dataset_u
        self.n_classes   = n_classes

        del image_data

    
    def next_data(self, n_way, n_shot, n_query, num_unlabel=0, n_distractor=0, train=True):
        """
            get support,query,unlabel data from n_way
            get unlabel data from n_distractor
        """
        support          = np.zeros([n_way, n_shot, self.im_height, self.im_width, self.channels], dtype=np.float32)
        query            = np.zeros([n_way, n_query, self.im_height, self.im_width, self.channels], dtype=np.float32)
        if num_unlabel>0:
            unlabel      = np.zeros([n_way+n_distractor, num_unlabel, self.im_height, self.im_width, self.channels], dtype=np.float32)
        else:
            unlabel      = []
            n_distractor = 0

        selected_classes = np.random.permutation(self.n_classes)[:n_way+n_distractor]
        for i, cls in enumerate(selected_classes[0:n_way]): # train way
            # labled data
            idx1         = np.random.permutation(self.n_label)[:n_shot + n_query]
            support[i]   = self.dataset_l[cls, idx1[:n_shot]]
            query[i]     = self.dataset_l[cls, idx1[n_shot:]]
            # unlabel
            if num_unlabel>0:
                idx2        = np.random.permutation(self.n_unlabel)[:num_unlabel]
                unlabel[i]  = self.dataset_u[cls,idx2]

        for j,cls in enumerate(selected_classes[self.n_classes:]): # distractor way
            idx3            = np.random.permutation(self.n_unlabel)[:num_unlabel]
            unlabel[i+j]    = self.dataset_u[cls,idx3]

        support_labels      = np.tile(np.arange(n_way)[:, np.newaxis], (1, n_shot)).astype(np.uint8)
        query_labels        = np.tile(np.arange(n_way)[:, np.newaxis], (1, n_query)).astype(np.uint8)
        # unlabel_labels = np.tile(np.arange(n_way+n_distractor)[:, np.newaxis], (1, num_unlabel)).astype(np.uint8)
        

        return support, support_labels, query, query_labels, unlabel



