# coding: utf-8
from   __future__ import print_function
import numpy as np
#import tensorflow as tf
import tensorflow.compat.v1 as tf
tf.disable_v2_behavior()
import tensorflow.compat.v1.keras.layers as layers

class models(object):
    def __init__(self, args):
        # parameters
        self.im_width, self.im_height, self.channels = list(map(int, args['x_dim'].split(',')))
        self.h_dim, self.z_dim = args['h_dim'], args['z_dim']
        self.args              = args
        #self.im_size = np.ceil(self.im_width / 16, dtype=int)

        # placeholders for data and label
        self.x      = tf.placeholder(tf.float32, [None, None, self.im_height, self.im_width, self.channels])
        self.ys     = tf.placeholder(tf.int64, [None, None])
        self.q      = tf.placeholder(tf.float32, [None, None, self.im_height, self.im_width, self.channels])
        self.y      = tf.placeholder(tf.int64, [None, None])
        self.phase  = tf.placeholder(tf.bool, name='phase')
        
        self.alpha  = args['alpha']
#        self.resnet = ResNet12(conv_cut = True)

    def resnet_block(self, x, kernel_size, filters, conv_cut=True, name = 'resblock', reuse=False):
        with tf.variable_scope(name, reuse=reuse):
            x_ori = x
            #conv1
            out = tf.layers.conv2d(x, filters, kernel_size, padding='SAME', name=name + '_1_conv', kernel_initializer='he_normal', kernel_regularizer = tf.keras.regularizers.l2(0.01), use_bias = False)
            out = tf.layers.batch_normalization(out, epsilon=1.001e-5, name=name + '_1_bn', training = self.phase)
            out = tf.nn.leaky_relu(out)
            
            #conv2 = tf.layers.conv2d(filters, kernel_size, padding='SAME', name=name + '_2_conv')
            out = tf.layers.conv2d(out, filters, kernel_size, padding='SAME', name=name + '_2_conv', kernel_initializer='he_normal', kernel_regularizer = tf.keras.regularizers.l2(0.01), use_bias = False)
            out = tf.layers.batch_normalization(out, epsilon=1.001e-5, name=name + '_2_bn', training = self.phase)
            out = tf.nn.leaky_relu(out)

            #conv3
            out = tf.layers.conv2d(out, filters, kernel_size, padding='SAME', name=name + '_3_conv', kernel_initializer='he_normal', kernel_regularizer = tf.keras.regularizers.l2(0.01), use_bias = False)
            out = tf.layers.batch_normalization(out, epsilon=1.001e-5, name=name + '_3_bn', training = self.phase)
            
            #short cut
            if conv_cut:
                shortcut = tf.layers.conv2d(x_ori, filters, 1, name=name + '_0_conv', kernel_initializer='he_normal', kernel_regularizer = tf.keras.regularizers.l2(0.01), use_bias = False)
                shortcut = tf.layers.batch_normalization(shortcut, epsilon=1.001e-5, name=name + '_0_bn', training = self.phase)
            else:
                shortcut = x_ori
            
            out += shortcut
            out = tf.nn.leaky_relu(out)
            
            out = tf.layers.MaxPooling2D(pool_size = (2, 2), strides=2)(out)

            return out

    def resnet(self, x, channel_list = [64,128,256,64], conv_cut=True, reuse=False):
        with tf.variable_scope('resnet', reuse=reuse):
            x = self.resnet_block(x, kernel_size = 3, filters = channel_list[0], conv_cut = conv_cut, name = 'resblock1')
            x = self.resnet_block(x, kernel_size = 3, filters = channel_list[1], conv_cut = conv_cut, name = 'resblock2')
            x = self.resnet_block(x, kernel_size = 3, filters = channel_list[2], conv_cut = conv_cut, name = 'resblock3')
            x = self.resnet_block(x, kernel_size = 3, filters = channel_list[3], conv_cut = conv_cut, name = 'resblock4')
            return x


    def conv_block(self, inputs, out_channels, pool_pad='VALID', name='conv'):
        with tf.variable_scope(name):
            conv = tf.layers.conv2d(inputs, out_channels, kernel_size=3, padding="same")
            #conv = tf.contrib.layers.batch_norm(conv, is_training=self.phase, decay=0.999, epsilon=1e-3, scale=True, center=True)
            conv = tf.layers.batch_normalization(conv, epsilon=1e-3, training = self.phase)
            conv = tf.nn.relu(conv)
            #conv = tf.contrib.layers.max_pool2d(conv, 2, padding=pool_pad)
            conv = tf.layers.MaxPooling2D(pool_size = (2, 2), strides=2, padding = pool_pad)(conv)

            return conv


    def encoder(self, x, h_dim, z_dim, reuse=False):
        """Feature embedding network"""
        with tf.variable_scope('encoder', reuse=reuse):
            net = self.conv_block(x,   h_dim, name='conv_1')
            net = self.conv_block(net, h_dim, name='conv_2')
            net = self.conv_block(net, h_dim, name='conv_3')
            net = self.conv_block(net, z_dim, name='conv_4')
            
            
            #net = tf.contrib.layers.flatten(net)
            net = tf.keras.layers.Flatten()(net)

            return net


    def feat_select(self, x, reuse=False):
        """Feature embedding network"""
        with tf.variable_scope('feat_select', reuse=reuse):
            #----
            num_filter = 32
            N = self.args['n_way']*(self.args['n_shot'] + self.args['n_query'])

            net = tf.reshape(x, [N,self.args['im_size'],self.args['im_size'],64])#[N,5,5,64]
            feat_val = tf.transpose(net, [1,2,0,3])#[5,5,N,64]
            feat_val = tf.reshape(feat_val, [self.args['im_size']*self.args['im_size'],N,64,1])

            feat_val = tf.keras.layers.Conv2D(filters=64, kernel_size=(N,1),  strides=(1,1),padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(feat_val)#[25,1,64,64]
            feat_val = tf.keras.layers.Conv2D(filters=32, kernel_size=(1,1), strides=(1,1), padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(feat_val)#[25,1,64,32]
            feat_val = tf.keras.layers.Conv2D(filters=1, kernel_size=(1,1), strides=(1,1), padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(feat_val)#[25,1,64,1]
            feat_val = tf.reshape(feat_val, [self.args['im_size'],self.args['im_size'],-1])
            #feat_val = tf.nn.softmax(feat_val)
            net = tf.math.multiply(net, feat_val)
            net = tf.reshape(net, [N, self.args['im_size']*self.args['im_size']*64])
            return net, feat_val
            #----
    
    def conv_feat(self, net, N, win_size, name, reuse=False):
        with tf.variable_scope('conv_feat'+name, reuse=reuse):
            feat_val = tf.keras.layers.Conv2D(filters=64, kernel_size=(N,win_size),  strides=(N,1),padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(net)#[25,1,64,64]
            feat_val = tf.keras.layers.Conv2D(filters=32, kernel_size=(1,1), strides=(1,1), padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(feat_val)#[25,1,64,32]
            feat_val = tf.keras.layers.Conv2D(filters=1, kernel_size=(1,1), strides=(1,1), padding='valid',activation=tf.nn.leaky_relu, use_bias=False)(feat_val)#[25,1,64,1]
            feat_val = tf.reshape(feat_val, [self.args['im_size'],self.args['im_size'],-1])
            return feat_val


    def feat_select_multi(self, x, view_list, reuse=False):
        """Feature embedding network"""
        with tf.variable_scope('feat_select_multi', reuse=reuse):
            #----
            num_filter = 32
            N = self.args['n_way']*(self.args['n_shot'] + self.args['n_query'])
            net = tf.reshape(x, [N,5,5,64])#[N,5,5,64]
            net_reshape = tf.transpose(net, [1,2,0,3])#[5,5,N,64]
            net_reshape = tf.reshape(net_reshape, [25,N,64,1])

            #win-1
            feat_val = self.conv_feat(net_reshape, N, view_list[0], name = 'win1')
            net_one = tf.math.multiply(net, feat_val)
            

            #win-3
            feat_val = tf.pad(net_reshape, [[0,0],[0,0],[1,1],[0,0]])
            feat_val = self.conv_feat(feat_val, N, view_list[1], name = 'win3')
            net_three = tf.math.multiply(net, feat_val)#[N,5,5,64]
            
            #win-5
            feat_val = tf.pad(net_reshape, [[0,0],[0,0],[2,2],[0,0]])#[25,64,64,1]
            feat_val = self.conv_feat(feat_val, N, view_list[2], name = 'win5')
            net_five = tf.math.multiply(net, feat_val)

            net = tf.concat([net_one, net_three, net_five], axis = 3)#[N,5,5,64*2]
            net = tf.reshape(net, [N, 5*5*64*3])
            return net, feat_val

    def feat_select_support(self, x, view_list, reuse=False):
        """Feature embedding network"""
        with tf.variable_scope('feat_select_support', reuse=reuse):
            #----
            num_filter = 32
            num_all = self.args['n_way']*(self.args['n_shot'] + self.args['n_query'])
            net = tf.reshape(x, [-1,self.args['im_size'],self.args['im_size'],64])#[N,5,5,64]
            
            N =  self.args['n_way'] * self.args['n_shot']
            net_reshape = tf.transpose(net[0:N, :, :, :], [1,2,0,3])#[5,5,N,64]
            net_reshape = tf.reshape(net_reshape, [self.args['im_size']*self.args['im_size'],N,64,1])

            #win-1
            feat_val = self.conv_feat(net_reshape, N, view_list[0], name = 'win1')
            net = tf.math.multiply(net, feat_val)
            
            net = tf.reshape(net, [num_all, self.args['im_size']*self.args['im_size']*64])
            return net, feat_val




    def relation(self, x, h_dim, z_dim, last_d, reuse=False):
        """Graph Construction Module"""
        with tf.variable_scope('relation', reuse=reuse):
            x   = tf.reshape(x, [-1,self.args['im_size'],self.args['im_size'],last_d])

            net = self.conv_block(x,    h_dim,   pool_pad='SAME', name='conv_5')
            net = self.conv_block(net,  1,       pool_pad='SAME', name='conv_6')
        
#            net = tf.contrib.layers.flatten(net)
#            net = tf.contrib.layers.fully_connected(net, 8)
#            net = tf.contrib.layers.fully_connected(net, 1, tf.identity)
#            net = tf.contrib.layers.flatten(net)

            net = tf.keras.layers.Flatten()(net)
            net = tf.keras.layers.Dense(8, activation=tf.nn.relu)(net)
            net = tf.keras.layers.Dense(1, activation=tf.nn.relu)(net)
            net = tf.keras.layers.Flatten()(net)

            return net


    # contruct the model
    def construct(self):
        # data input
        x_shape                  = tf.shape(self.x)
        q_shape                  = tf.shape(self.q)
        num_classes, num_support = x_shape[0], x_shape[1]
        num_queries              = q_shape[1]

        ys_one_hot = tf.one_hot(self.ys, depth=num_classes)
        y_one_hot  = tf.one_hot(self.y,  depth=num_classes)

        # construct the model
        x       = tf.reshape(self.x, [num_classes * num_support, self.im_height, self.im_width, self.channels])
        q       = tf.reshape(self.q, [num_classes * num_queries, self.im_height, self.im_width, self.channels])
        if self.args['encoder'] == 'conv':
            emb_x   = self.encoder(x, self.h_dim, self.z_dim)
            emb_q   = self.encoder(q, self.h_dim, self.z_dim, reuse=True)
        else:
            #emb_x = self.resnet(x, training = self.phase)
            #emb_q = self.resnet(q, training = self.phase)
            emb_x = self.resnet(x)
            emb_q = self.resnet(q, reuse=True)

        if self.args['rn']==300:       # learned sigma, fixed alpha
            self.alpha = tf.constant(self.args['alpha'])
        else:                          # learned sigma and alpha
            self.alpha = tf.Variable(self.alpha, name='alpha')
        
        if self.args['infer'] == 'tpn':
            ce_loss, acc, sigma_value, debug_dim = self.label_prop(emb_x, emb_q, ys_one_hot)
        else:
            ce_loss, acc, sigma_value, debug_dim = self.prototype(emb_x, emb_q, ys_one_hot)


        return ce_loss, acc, sigma_value, debug_dim
    
    def prototype(self, x, u, ys):
        epsilon = np.finfo(float).eps
        # x: NxD, u: UxD

        s       = tf.shape(ys)
        ys      = tf.reshape(ys, (s[0]*s[1],-1))
        Ns, C   = tf.shape(ys)[0], tf.shape(ys)[1]
        
        Nu      = tf.shape(u)[0]
        yu      = tf.zeros((Nu,C))/tf.cast(C,tf.float32) + epsilon  # 0 initialization

        y       = tf.concat([ys,yu],axis=0)
        gt      = tf.reshape(tf.tile(tf.expand_dims(tf.range(C),1), [1,tf.cast(Nu/C,tf.int32)]), [-1])
        
        all_un  = tf.concat([x,u],0)
        all_un  = tf.reshape(all_un, [-1, self.args['im_size']*self.args['im_size']*64])
        N, d    = tf.shape(all_un)[0], tf.shape(all_un)[1]
        
        if self.args['feat_select'] == 'single':
            last_d = 64
            all_un, feat_select = self.feat_select(all_un, reuse=False)
        elif self.args['feat_select'] == 'multi':
            last_d = 64*3
            all_un, feat_select = self.feat_select_multi(all_un, view_list=[1,3,5], reuse=False)
        elif self.args['feat_select'] == 'support':
            last_d = 64
            all_un, feat_select = self.feat_select_support(all_un, view_list=[1,3,5], reuse=False)
        else:
            last_d = 64
            feat_select = tf.constant([0.1, 0.1])

        #distance and label
        emb_s = all_un[0:Ns, :]#[Ns,f]
        emb_s = tf.reshape(emb_s, [C, self.args['n_shot'], -1])#[n_way,n_shot,f]
        emb_s = emb_s[:, 0: int(self.args['semi_ratio']*self.args['n_shot']), :]
        emb_s = tf.reduce_mean(emb_s, axis = 1)#[n_way,n_shot,f]->[n_way,f]

        emb_q = all_un[Ns:, :]
        
        emb_s = tf.expand_dims(emb_s, axis=0)#[1,n_way,f]
        emb_q = tf.expand_dims(emb_q, axis=1)#[Nq,1,f]
        dist = tf.reduce_mean(tf.math.square(emb_q - emb_s), axis = 2)#[Nq,n_way,f]->[Nq,n_way]
        
        F = tf.nn.log_softmax(-dist)#[Nq,n_way]
        pred_label = tf.argmax(F, 1)#[Nq,]
        
        #compute loss
        q_one_hot   = tf.reshape(tf.one_hot(tf.cast(gt, tf.int64),depth=C),[Nu, -1])
        ce_loss     = -q_one_hot*F#[nq,n_way]
        ce_loss     = tf.reduce_mean(tf.reduce_sum(ce_loss,1))
        
        # only consider query examples acc
        F_un        = F
        acc         = tf.reduce_mean(tf.to_float(tf.equal(pred_label,tf.cast(gt,tf.int64))))

        return ce_loss, acc, tf.constant([0.1,0.1]), all_un#[F[0:1, :], q_one_hot[0:1, :], ce_loss]
    #[F[0:1, :], q_one_hot[0:1, :], ce_loss]
    #[emb_s[:, 0, 0:3], emb_q[0,:,0:3], ce_loss]

    def label_prop(self, x, u, ys):

        epsilon = np.finfo(float).eps
        # x: NxD, u: UxD
        s       = tf.shape(ys)
        ys      = tf.reshape(ys, (s[0]*s[1],-1))
        Ns, C   = tf.shape(ys)[0], tf.shape(ys)[1]
        


        Nu      = tf.shape(u)[0]
        yu      = tf.zeros((Nu,C))/tf.cast(C,tf.float32) + epsilon  # 0 initialization


        y       = tf.concat([ys,yu],axis=0)
        gt      = tf.reshape(tf.tile(tf.expand_dims(tf.range(C),1), [1,tf.cast(Nu/C,tf.int32)]), [-1])
        
        all_un  = tf.concat([x,u],0)
        all_un  = tf.reshape(all_un, [-1, self.args['im_size']*self.args['im_size']*64])
        N, d    = tf.shape(all_un)[0], tf.shape(all_un)[1]
        
        if self.args['feat_select'] == 'single':
            last_d = 64
            all_un, feat_select = self.feat_select(all_un, reuse=False)
        elif self.args['feat_select'] == 'multi':
            last_d = 64*3
            all_un, feat_select = self.feat_select_multi(all_un, view_list=[1,3,5], reuse=False)
        elif self.args['feat_select'] == 'support':
            last_d = 64
            all_un, feat_select = self.feat_select_support(all_un, view_list=[1,3,5], reuse=False)
        else:
            last_d = 64
            feat_select = tf.constant([0.1, 0.1])
        #print('last_d:{} all_unshape: {}'.format(last_d, tf.shape(all_un)))

        # compute graph weights
        if self.args['rn'] in [30, 300]:   # compute example-wise sigma
            self.sigma = self.relation(all_un, self.h_dim, self.z_dim, last_d)
            all_un  = all_un / (self.sigma+epsilon)
            all1    = tf.expand_dims(all_un, axis=0)
            all2    = tf.expand_dims(all_un, axis=1)
            W       = tf.reduce_mean(tf.square(all1-all2), axis=2)
            W       = tf.exp(-W/2)

        # kNN Graph
        if self.args['k']>0:
            W = self.topk(W, self.args['k'])
        
        # Laplacian norm
        D           = tf.reduce_sum(W, axis=0)
        D_inv       = 1.0/(D+epsilon)
        D_sqrt_inv  = tf.sqrt(D_inv)

        # compute propagated label
        D1          = tf.expand_dims(D_sqrt_inv, axis=1)
        D2          = tf.expand_dims(D_sqrt_inv, axis=0)
        S           = D1*W*D2
        F           = tf.matrix_inverse(tf.eye(N)-self.alpha*S+epsilon)
        F           = tf.matmul(F,y)
        label       = tf.argmax(F, 1)

        # loss computation
        F = tf.nn.softmax(F)
        
        y_one_hot   = tf.reshape(tf.one_hot(tf.cast(gt, tf.int64),depth=C),[Nu, -1])
        y_one_hot   = tf.concat([ys,y_one_hot], axis=0)
        
        ce_loss     = y_one_hot*tf.log(F+epsilon)
        ce_loss     = tf.negative(ce_loss)
        ce_loss     = tf.reduce_mean(tf.reduce_sum(ce_loss,1))
        
        # only consider query examples acc
        F_un        = F[Ns:,:]
        acc         = tf.reduce_mean(tf.to_float(tf.equal(label[Ns:],tf.cast(gt,tf.int64))))

        return ce_loss, acc, tf.constant([0.1,0.1]), feat_select#all_un

    
    def topk(self, W, k):
         # construct k-NN and compute margin loss
        values, indices     = tf.nn.top_k(W, k, sorted=False)
        my_range            = tf.expand_dims(tf.range(0, tf.shape(indices)[0]), 1) 
        my_range_repeated   = tf.tile(my_range, [1, k])
        full_indices        = tf.concat([tf.expand_dims(my_range_repeated, 2), tf.expand_dims(indices, 2)], axis=2)
        full_indices        = tf.reshape(full_indices, [-1, 2])
        
        topk_W  = tf.sparse_to_dense(full_indices, tf.shape(W), tf.reshape(values, [-1]), default_value=0., validate_indices=False)
        ind1    = (topk_W>0)|(tf.transpose(topk_W)>0) # union, k-nearest neighbor
        ind2    = (topk_W>0)&(tf.transpose(topk_W)>0) # intersection, mutal k-nearest neighbor
        ind1    = tf.cast(ind1,tf.float32)

        topk_W  = ind1*W
        
        return topk_W


